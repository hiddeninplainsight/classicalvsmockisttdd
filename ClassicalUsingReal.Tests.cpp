#include "Order.hpp"
#include "Warehouse.hpp"
#include <gmock/gmock.h>

using namespace ::testing;

TEST(Order_ClassicalUsingReal, Filling_an_order_removes_the_items_from_the_inventory)
{
	Warehouse inventory;
	inventory.AddInventory(50, "Apples");

	Order target(20, "Apples");
	target.Fill(inventory);

	EXPECT_EQ(30, inventory.HowManyInStock("Apples"));
}


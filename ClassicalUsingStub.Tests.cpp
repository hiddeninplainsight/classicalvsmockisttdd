#include "Order.hpp"
#include "RemovableInventory.hpp"
#include <gmock/gmock.h>

using namespace ::testing;

class RemovableInventoryStub : public RemovableInventory
{
public:
	mutable int hasNumberOf;
	mutable std::string hasItem;

	int removeNumberOf;
	std::string removeItem;

	RemovableInventoryStub() : 
		hasNumberOf(0), hasItem(""), removeNumberOf(0), removeItem("")
	{
	}

	virtual bool HasInventory(int numberOf, const std::string &item) const
	{
		hasNumberOf = numberOf;
		hasItem = item;
		return true;
	}

	virtual void RemoveInventory(int numberOf, const std::string &item)
	{
		removeNumberOf = numberOf;
		removeItem = item;
	}
};

TEST(Order_ClassicalUsingStub, Filling_an_order_removes_the_items_from_the_inventory)
{
	RemovableInventoryStub inventory;

	Order target(20, "Apples");
	target.Fill(inventory);

	EXPECT_EQ(20, inventory.hasNumberOf);
	EXPECT_EQ("Apples", inventory.hasItem);

	EXPECT_EQ(20, inventory.removeNumberOf);
	EXPECT_EQ("Apples", inventory.removeItem);
}


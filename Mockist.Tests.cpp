#include "Order.hpp"
#include "RemovableInventory.hpp"
#include <gmock/gmock.h>

using namespace ::testing;

class RemovableInventoryMock : public RemovableInventory
{
public:
	MOCK_CONST_METHOD2(HasInventory, bool(int numberOf, const std::string &item));
	MOCK_METHOD2(RemoveInventory, void(int numberOf, const std::string &item));
};

TEST(Order_Mockist, Filling_an_order_removes_the_items_from_the_inventory)
{
	RemovableInventoryMock inventory;

	EXPECT_CALL(inventory, HasInventory(20, "Apples"))
		.Times(1)
		.WillOnce(Return(true));

	EXPECT_CALL(inventory, RemoveInventory(20, "Apples"))
		.Times(1);

	Order target(20, "Apples");
	target.Fill(inventory);
}


#ifndef ORDER_H
#define ORDER_H

#include <string.h>
#include "RemovableInventory.hpp"

class Order
{
public:
	Order(int numberOf, std::string item);
	void Fill(RemovableInventory &inventory);
};

#endif


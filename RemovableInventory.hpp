#ifndef REMOVABLE_INVENTORY
#define REMOVABLE_INVENTORY

#include <string>

class RemovableInventory
{
public:
	virtual ~RemovableInventory() { }

	virtual bool HasInventory(int numberOf, const std::string &item) const = 0;
	virtual void RemoveInventory(int numberOf, const std::string &item) = 0;
};

#endif


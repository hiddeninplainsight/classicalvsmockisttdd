#include "Warehouse.hpp"

Warehouse::Warehouse() :
	numberOfItems(0),
	itemName("")
{
}

void Warehouse::AddInventory(int numberOf, const std::string &)
{
	numberOfItems += numberOf;
}

bool Warehouse::HasInventory(int numberOf, const std::string &) const
{
	return numberOfItems >= numberOf;
}

void Warehouse::RemoveInventory(int numberOf, const std::string &)
{
	numberOfItems -= numberOf;
}

int Warehouse::HowManyInStock(const std::string &item)
{
	return numberOfItems;
}


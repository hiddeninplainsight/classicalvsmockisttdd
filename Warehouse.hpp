#ifndef WAREHOUSE_H
#define WAREHOUSE_H

#include <string.h>
#include "RemovableInventory.hpp"

class Warehouse : public RemovableInventory
{
private:
	int numberOfItems;
	std::string itemName; 
public:
	Warehouse();
	virtual ~Warehouse() { };

	void AddInventory(int numberOf, const std::string &item);
	virtual bool HasInventory(int numberOf, const std::string &item) const;
	virtual void RemoveInventory(int numberOf, const std::string &item);
	int HowManyInStock(const std::string &item);
};

#endif

